# ElasticSearch

ElasticSearch is open-source engine built on Apache Lucene using Java language. It allows one to store, analyze and search huge volumes of data and return the efficient answers in very quickly.It is able to achieve such fast responses because it searches an index instead of actual text data.

Elasticsearch can also be seen as a distributed, RESTful search engine optimized for speed and relevance on production-scale workloads.

## How ElasticSearch works

ElasticSearch orgnaizes data into documents, which are then grouped into indices based on their characteristics. It uses inverted indices to for efficient search. Inverted indices are data structures that maps word to their locations thus making it easy to find the documents quickly. It's distributed nature helps to do rapid search and analyze huge volumes of data with real-time performance.

## Indices and Inverted Index

Index is basically a collection of documents which have similar characteristics. The documents inside the index is logically related. An index is identified by a name that is used to refer to the index while searching, deleting documents in it.

Inverted index is basically the index in ElasticSearch. It is the mechanism by which all search engines work. It is basically like a hashmap that lets one map from word to the document. Inverted Index splits each documents into individual search terms such as words then maps each search terms to corresponding documents. 

## Backend Components

### Cluster

ElasticSearch cluster is a group of one or more node instances that are connected together.

### Node

Node is a single server that is part of the cluster and stores data.

# Solr

Solr or Apache Solr stands for Searching On Lucene w/ Replication. It is a free and open-source search engine which is also based on Apache Lucene library.
Solr is often used as a document based NoSQL database that can be used to store even key-value data.

## Advantages

- Solr is a stable, reliable and tolerant-free search platform.
- Solr can be used to improve user experience.
- Powerful Full-Text Search capabilities.
- High scalability and flexibility.
- Easy monitoring.

# Lucene

Lucene or Apache Lucene is free and open source full-featured text search engine library written in Java. It is still one of the most active projects in Apache. It allows users to add search capablities to applications and websites. It takes data and adds it to text index and can be used to perform queries.
The elements that Lucene is interested in can be segmented into fields, containing information and keywords, such as author names, titles and file names.

## Uses

- It can work without Internet.
- It can work with archives, libraries and PC servers.
- It can handle and search HTML, documents, emails and pdf.

## Core of Lucene

Core of Lucene is index, which is vital for search cause all the documents are stored in it. The elements can found out by moving all the documents to the index list. The elements then can be segmented into fields. Once all elements are gathered, tokenization can be done to create segments. The most popular tokenization is the white space strategy which seperates each word by removing white spaces.

[elasticsearch](https://www.knowi.com/blog/what-is-elastic-search/)
[solr](https://sematext.com/guides/solr/)
[lucene](https://techmonitor.ai/technology/data/what-is-apache-lucene)