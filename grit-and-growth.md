# Grit

## Paraphrase (summarize) the video in a few lines. Use your own words.

These are some of the important things I understood after watching the video :

- Passion, determination and perseverance are more important than skill, IQ, and intelligence. 
- People who have passion and perseverance otherwise known as gritty people will achieve much more in their respective fields compared to the geniuses who barely put in any effort to achieve results in a long term.

## What are your key takeaways from the video to take action on?

These are some of the things that I have to take action on:
 
- Try to find a subject or area in which I am more passionate on.
- Try new things inspite of failure.
- Have to be more determined to achieve my goals.
- Have to understand what failure is and how it will be stepping stones to success.

# Introduction to Growth Mindset

## Paraphrase (summarize) the video in a few lines in your own words.

There is a massive difference in the impact created by mindset and the best examples were shown in the video. There are mainly two types of mindset, which are fixed mindset and growth mindset. These are some of the differences between people with fixed mindset and growth mindset:

- People with fixed mindset are not open to looking for opportunities to grow and instead believe that a person is born with defined skills and talent.
- People with growth mindset are open to challenges as they believe that it will help them grow.
- People with fixed mindset view responses or feedback as personal and does not view it as advices.
- People with growth mindset view mistakes as stepping stones to learn and understand more about the topic that they want to improve in.

## What are your key takeaways from the video to take action on?

These are some of the things that I have to take action on:

- View challenges as opportunities to grow and develop skills.
- View mistakes as opportunities to learn and grow.
- Try to understand and give ear to more feedbacks from friends and family.
- Try to surround myself with people having growth mindset.

# Understanding Internal Locus of Control

## What is the Internal Locus of Control? What is the key point in the video?

Internal locus of control is the mindset that thinks most of things that happen around a person is because of their actions. Having internal locus of control is much more motivating and allows us to think in such a way that improves our effort on everything that is happening around us.

People with internal locus of control have the ability to adapt and survive difficult situations compared to people with external locus of control.
People with internal locus of control can be viewed as people with growth mindset and people with external locus of control can be viewed as people with fixed mindset.

# How to build a Growth Mindset

## Paraphrase (summarize) the video in a few lines in your own words.

Open mindedness about failures, challenges, negative remarks and struggles that one face is a very important aspect in learning and having a growth mindset. Having a growth mindset not only helps one in one particular topic, it will help one to improve in all aspects of life in general in long term.

Understanding that failure does not mean the end and it is a chance to improve from our mistake is a major part of learning. 

## What are your key takeaways from the video to take action on?

These are some of the things that I have to take action on:

- Learn from mistakes to improve more on the topic.
- Listen to what others have to say about me and my actions.
- Learn to face more challenges and try to increase perseverance.

# Mindset - A MountBlue Warrior Reference Manual

## What are one or more points that you want to take action on from the manual? (Maximum 3)

These are some of the things that I have to take action on:

- I will stay relaxed and focused no matter what.
- I am 100 percent responsible for my learning.
- I know more efforts lead to better understanding.





