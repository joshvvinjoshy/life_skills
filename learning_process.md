# How to Learn Faster with Feynman Technique

## What is the Feynman Technique? Paraphrase the video in your own words.

Feynman technique is one of the most effective techniques that enables one to learn a particular topic or subject faster. It consists of four steps:
* Writing the name of the concept on a piece of paper.
* Trying to write the description of the process in simple words that anyone new to the subject can understand(as if one is teaching a little child).
* Trying to find the area where proper explanation could not be given and revisit those areas and learn that particular topic as much as one can.
* Find areas where simple explanation could not be given and only complex terms and methods where used. This means that one did not properly understand that particular area or topic.

## What are the different ways to implement this technique in your learning process?

Feynman technique can be implemented using different ways. Since my learning process mainly consists of learning data types, functions and algorithms, it would consist of learning a particular function and trying to explain that function to a peer who is fairly new to the topic and making the peer understand that function. If I was not able to make the peer understand, then that means that I was not able to grasp that particular function properly. What I can do is to revisit those areas where I was not able to explain in simple terms and try to understand the topic in such a way that I can explain it to anybody(even a complete beginner in Computer Science).

# Learning How to Learn TED talk by Barbara Oakley

## Paraphrase the video in detail in your own words.

Learning how to re-wire the brain otherwise known as changing the thought process is very challenging and very hard. Human minds are simple yet complex as so many thoughts come across our minds when we start to learn something, which need not be related to the topic that we are trying to learn. Human mind is simple in a way that we almost do the daily processes in a particular way, the way that is unique for everyone. For example, the way one thinks is almost the same for a particular situation. If we are able to store the decisions that we have taken for a particular situations, we can most definitely predict our decisions in the future in the same situations using history. It is because our thought process is almost the same. Learning how to learn is one of most important things one can learn in this world, cause it can effect everything. 

## What are some of the steps that you can take to improve your learning process?

The steps that I can take to improve my learning process are as follows:
* Focusing more on the basics of the topic.
* Understanding the different scenerios of the particular topic or function.

# Learn Anything in 20 hours

## Your key takeaways from the video? Paraphrase your understanding.

Any skill can be learned to a considerably good level in focused 20 hours of practice. Most of the time when one tries to learn a new skill, one do not focus and practice. And that is why one takes a considerable huge amount of time to learn even the basics of the skill. It also includes other things such as understanding where one's weak points are and reserving time for learning the areas where one is weak.

## What are some of the steps that you can while approaching a new topic?

The steps that I can follow while approaching new topic are as follows:
* Understand the basics properly.
* Understand the purpose of learning the topic.
* Understand how the topic relates to the already learned topics.

