# What are the steps/strategies to do Active Listening?

## Steps to do Active listening are as follows:

* Do not get distracted by ur own thoughts.
* Focus on the speaker and topic.
* Try not to interrupt the speaker as it will disrupt the flow of thought process of communication.
* Try to show interest in the topic.
* Show that you are listening using body language.
* Paraphrase what speaker has said to confirm that you are on the same page.

# According to Fisher's model, what are the key points of Reflective Listening?

## The key points of Reflective Listening are :

* Always confirm that you are on the same page as the speaker.
* It is important to keep their tone as well while responding back to the speaker.
* There must be sincere interest while listening to the speaker.
* It is a form of expression with genuine understanding of the topic or the situation.

# What are the obstacles in your listening process?

## Obstacles in my listening process is the same as others. They are:

* Not interested in the subject.
* Do not know much about the subject.
* Get distracted by the things happening around me.
* Get distracted by many non-worldly ideas, which were created mid conversation.
* Easily get bored to particular topics.

# What can you do to improve your listening?

## These are some of the steps that I thought would be good to improve my listening:

* Understanding the purpose of the conversation.
* Understanding what it means to the speaker.
* Try to show interest even if I don't like the topic, cause the topic might be interesting to the speaker.
* Try to focus on the speaker to not get distracted by ideas popping inside my head.

# When do you switch to Passive communication style in your day to day life?

## These are some situations in which I switch to Passive communication:

* When I am with a person who really likes to talk.
* When I am in an argument which doesn't really mean anything to me.
* When I am really sleepy cause I don't really care who wins the argument, I just want to sleep.

# When do you switch into Aggressive communication styles in your day to day life?

## These are some situations in which I switch to Aggressive communication:

* When I am really irritated about the topic or the speaker.
* When the speaker starts disrespecting me or the things I like about.
* When the speaker doesn't take hint that I am not interested in the topic and continue to their story.(I tend to give many hints to speaker if I dont like the topic)

# When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

## These are some situations in which I switch into Passive Aggressive communication:

* When I have been hurt by someone emotionally or verbally and I know that person is not a good person.

# How can you make your communication assertive? You can analyse the videos and then think what steps you can apply in your own life?

## These are some of the steps that I can apply in my life to make my communication more assertive:

* Try to express feelings as it is rather than bending them in such a way to make communication frictionless.
* Try to voice ideas and opinions as it is.
