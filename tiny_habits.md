# 1. Tiny Habits - BJ Fogg

## Your takeaways from the video (Minimum 5 points)

These are some of my takeways from the video :

- Practicing tiny habits for long periods of time will definitely have a huge impact in the future. 
- The importance of tiny habits is that it is easy to start and make it a habit. 
- The thought of making and developing a habit is kind of exhausting. 
- Tiny habits if started or planted in the right place will eventually grow towards something huge in one's life.
- Best kind of motivation to start or develop tiny habit is do it after one's normal habit in the normal frequency.

# 2. Tiny Habits by BJ Fogg - Core Message

## Your takeaways from the video in as much detail as possible 

These are some of my takeways from the video :

- Hard tasks need high levels of motivation to fullfill the task.
- Most of the time one won't have the motivation to start or develop new habits, so relying only on the motivation to develop new habits is foolish.
- Most of the people use external or internal prompts to remind oneself of the habit and that is why people don't develop such habits.
- It is important to look for ways to develop habits with minimum level of motivation as one can find minimum level of motivation almost everytime.
- It is important to be consistent while starting or developing habits. Without consistency there is no use in starting the habit.

## How can you use B = MAP to make making new habits easier?

Most of the time my motivation levels are low. As motivation is almost non existent, it is hard to develop new habits. I'll have to look towards action prompts that are almost as same as the habit that I wnat to develop. I think it will easier to develop new habits if they are somehow linked to my previous or existing habits. Since behaviour is linked to motivation, ability and prompt and prompt is the easiest to use to develop new habits. Ability is also closely linked to motivation and since most of the time motivation levels are low, it is hard to depend entirely on abilities also. I already have some productive habits that I do regularly. I just have to find something that I want to achieve and think of a habit that I can start and develop alongside the already existing habits.

## Why it is important to "Shine" or Celebrate after each successful completion of habit?

It is important to celebrate after each successful completion of habit because of the following reasons:

- It will give us motivation to do it again the next time.
- It will remind our subconscious mind of the reward that one got the previous time.
- It will help us develop more difficult tasks or habits in the future.

# 3. 1% Better Every Day Video

## Your takeaways from the video (Minimum 5 points)

These are some of my takeways from the video :

- One step a day can make a huge change in one's life.
- Aggregation of marginal changes can lead to success.
- Good things make more time for you and bad things take time from you.
- When one process is repeated over many times, there is high probability to master it.
- Consistency is the most important thing whether it be developing a skill or habit.

## Write about the book's perspective on habit formation from the lens of Identity, processes and outcomes?

These are some of the book's perspective on habit formation :

- Understand that one cannot depend on motivation always.
- Understand the importance of action prompts.
- Consistency is one of the most important things that one need to develop a habit.
- It will be easier for one to develop habits if one is relying on action prompts.
- One can develop a habit much easier than others if one has proper plan to implement it.

## Write about the book's perspective on how to make a good habit easier?

These are some of the ways according to book's perspective to make a good habit easier:

- Depend on action prompts to build habits.
- Try to build habits that are easy to maintain and need minimum level of motivation to do so.
- Try to build tiny habits that can be done within 1 or 2 minutes.
- Try to be consistent as it is one of the most important thing that most people forget.

## Write about the book's perspective on making a bad habit more difficult?

These are some of the ways according to book's perspective to make a bad habit more difficult:

- Try to surround yourself with people who advice and motivate to be a better person.
- Try to make life busier to remove the time for bad habits.
- Try to develop new good habits that can eventually get rid of bad habits.


# 5. Reflection

## Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

I would like to do more of exercising and the steps that I would take to make it easier are:

- Surround myself with people who loves working out.
- Watch videos of famous athletes to get some kind of motivation.
- Try to stop habits that make me lazy and demotivated.


## Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

I would like to stop complaining of how hard life is for me and these are some of the steps that I would take to make it easier:

- Surround myself with people who motivate me to never give up and continue fighting against everything.
- Try to remember the struggles and challenges that I faced to reach this point of my life to kind of motivate myself.
- Understand what it would mean to my family and friends when I finally reach my goal.


