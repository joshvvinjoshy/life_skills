# What kinds of behaviour cause sexual harassment?

Various kinds of behaviour can cause sexual harassment which are generally classified into three types, they are :

## Verbal Harassment

These are the types of harassment that is done verbally. It includes comments about a person's body, jokes on gender and sex, comments suggesting or requesting sexual favors, and spreading rumours about a person's sexual life. 

## Visual Harassment

These are the types of harassment that can be seen generally on posters, comments on social media, drawings.

## Physical Harassment 

These are the types of harassment that are done physically. It generally includes inappropriate touching, sexual assault and sexual gesturing.

# What would you do in case you face or witness any incident or repeated incidents of such behaviour?

In case I face or witness any such kind of incident, I would report to higher authorities about the particular person or group of persons and about the incident that I faced or witnessed.
